import PySimpleGUI as sg
import os
from pytube import Channel, YouTube


# Layout 1 da janela, otém o canal e mostra o link dos vídeos dele
link_list_column = [
    [sg.Text('Link do canal'),
    sg.In(size=(30, 1), enable_events=True, key='-LINK-'),
    sg.Button('Listar', key='-LIST-')],
    [sg.Listbox(values=[], enable_events=True, size=(40, 20), key='-LINK LIST-')]
]

# Layout 2 da janela, mostrará o nome do vídeo selecionado e possibilitará o o download
video_view_column = [
    [sg.Text(size=(50, 1), key='-TEXT-')],
    [
        sg.In(size=(30, 1), enable_events=True, key='-FOLDER-', visible=False),
        sg.FolderBrowse(key='-FOLDER2-', visible=False),
        sg.Button('Download', key='-DOWNLOAD-', visible=False)
    ],
    [
        sg.Text('Resolução', key='-RES-', visible=False),
        sg.Listbox(values=[], key='-RES LIST-', visible=False)
    ]
]

# Layout completo
layout = [
    [sg.Column(link_list_column),
     sg.VSeparator('gray'),
     sg.Column(video_view_column)
     ]
]

window = sg.Window('YOUTUBE -> access', layout)

# Loop para verificar os eventos da janela
while True:
    event, values = window.read()
    if event == 'Exit' or event == sg.WIN_CLOSED:
        break

    if event == '-LIST-':
        try:
            channel = Channel(str(values))
            # links = [url for url in channel.video_urls]
            i = 1
            links = {}
            for url in channel.video_urls:
                sg.one_line_progress_meter('Carregando...', i, channel.__len__(), 'Aguarde o carregamento')
                i += 1
                links[YouTube(url).title] = url
        except:
            links = []
            print('Erro no evento -LIST-')

        window['-LINK LIST-'].update(links)
        window.set_title(f'Vídeos do canal --> {channel.channel_name}')

    elif event == '-LINK LIST-':
        try:
            video = YouTube(links[values['-LINK LIST-'][0]])
            sg.popup_timed('Aguarde um instante', auto_close_duration=10)
            # Filtrando apenas o formato mp4
            stream = video.streams.filter(mime_type='video/mp4')
            # Dicionário as resoluções dos vídeos
            res = {st.resolution: st.itag for st in stream}
            # caso não seja selecionado nenuma tag usa o valor já utlizado
            tag = 22

            window['-TEXT-'].update(video.title)
            window['-FOLDER-'].update(visible=True)
            window['-FOLDER2-'].update(visible=True)
            window['-DOWNLOAD-'].update(visible=True)
            window['-RES-'].update(visible=True)
            window['-RES LIST-'].update(visible=True)
            # Adiciona o dicionário ao box
            window['-RES LIST-'].update(res)

        except:
            print('Except -LINK LIST-')

    elif event == '-DOWNLOAD-':
        # Selecionando para download do item do fluxo cuja tag é 22, caso queria escolher seria
        # uma boa implementar uma lógica para tal
        stream = video.streams.get_by_itag(tag)
        # Fazendo o download
        stream.download()

    elif event == '-FOLDER-':
        # Alterando o diretório raiz
        try:
            os.chdir(values['-FOLDER-'])
        except:
            os.chdir('D:\\youtube\\video_youtube')

    elif event == '-RES LIST-':
        tag = res[values['-RES LIST-'][0]]

window.close()
